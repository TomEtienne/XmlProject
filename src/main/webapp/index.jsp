<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Projet XML - HAZARD Alexandre - ETIENNE Tom</title>
    <style>
        body {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100%;
            min-height: 100%;
            background: linear-gradient(135deg, #bcbfe5 0%,#95c2e5 100%);
        }
        img {
            width: auto;
            height: 150px;
            margin-bottom: 50px;
        }
        h1 {
            font-size: 72px;
            font-family: monospace;
        }
        h3 {
            text-align: center;
            font-family: monospace;
            font-size: 35px;
        }
    </style>
</head>
<body>
    <img src="logo_rouen.png" style="width: auto; height: 150px;" alt="Logo de l'Université de Rouen"/>
    <h1>Projet XML - Master G.I.L.</h1>
    <h3>HAZARD Alexandre<br/>
    ETIENNE Tom</h3>
</body>
</html>
