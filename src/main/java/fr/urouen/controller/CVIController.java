package fr.urouen.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.ws.http.HTTPException;

import org.apache.http.HttpException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import fr.urouen.model.CVS;
import fr.urouen.model.Cvi;

@RestController
public class CVIController {

	private Map<UUID,Cvi> bdd;
	
	@RequestMapping(value = "/resume", method = RequestMethod.GET)
	public @ResponseBody String getResume() {
		
		String sRet = "";
		
		for( Entry<UUID, Cvi> entry : bdd.entrySet()) {
			sRet += "<cv>";
			sRet += "<id>" +entry.getKey()+"</id>";
			sRet += "<nom>" + entry.getValue().getIdentite().getNom() + "</nom>";
			sRet += "<prenom>" + entry.getValue().getIdentite().getPrenom() + "</prenom>";
			if(entry.getValue().getObjectif().getStage() != null && entry.getValue().getObjectif().getStage().equals("")) {
				sRet += "<stage>" + entry.getValue().getObjectif().getStage() + "</stage>";
			} else {
				sRet += "<emploi>" + entry.getValue().getObjectif().getEmploi() + "</emploi>";
			}
			sRet += "</cv>";
		}
		
		return sRet;
	}
	
	@RequestMapping(value = "/resume/{id}", method = RequestMethod.GET)
	public @ResponseBody String getResumeCvId(@PathVariable("id") String id) {
		UUID uuid = null;
		String sRet = "";
		
		try {
			uuid = UUID.fromString(id);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		if(bdd.get(uuid) != null) {
			sRet += "<cv>";
			sRet += "<id>" + uuid + "</id>";
			sRet += "<nom>" + bdd.get(uuid).getIdentite().getNom() + "</nom>";
			sRet += "<prenom>" + bdd.get(uuid).getIdentite().getPrenom() + "</prenom>";
			if(bdd.get(uuid).getObjectif().getStage() != null && bdd.get(uuid).getObjectif().getStage().equals("")) {
				sRet += "<stage>" + bdd.get(uuid).getObjectif().getStage() + "</stage>";
			} else {
				sRet += "<emploi>" + bdd.get(uuid).getObjectif().getEmploi() + "</emploi>";
			}
			sRet += "</cv>";
		}
		
		return sRet;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteCvId(@PathVariable("id") String id) {
		UUID uuid = null;
		
		try {
			uuid = UUID.fromString(id);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		if(bdd.get(uuid) != null) {
			bdd.remove(uuid);
		}
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public @ResponseBody void updateCvId(@PathVariable("id") String id, @RequestBody String xml) {
		UUID uuid = null;
		
		try {
			uuid = UUID.fromString(id);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		if(bdd.get(uuid) != null) {
			Cvi cvi;
			try {
				cvi = unmarshal(xml);
				bdd.replace(uuid , cvi);
				System.out.println(cvi.toString());
			} catch (JAXBException e) {
				throw new HTTPException(500);
			}
		}
	}

	@RequestMapping(value = "/help")
	public RedirectView getHelp() {
		return new RedirectView("help.jsp");
	}

	@RequestMapping(value = "/")
	public RedirectView getIndex() {
		return new RedirectView("index.jsp");
	}
	
	

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public @ResponseBody String insert(@RequestBody String xml) {
		
		/*StreamSource xmlFile = new StreamSource(xml);
		URL xsd = getClass().getResource("/ProjetFileXSD.xsd");
		
		System.out.println("test 1");
		
		SchemaFactory schemaFactory = SchemaFactory
		    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
		  Schema schema = schemaFactory.newSchema(xsd);
		  System.out.println("test 2");
		  Validator validator = schema.newValidator();
		  System.out.println("test 3");
		  validator.validate(xmlFile);
		  System.out.println("test 4");
		  System.out.println(xmlFile.getSystemId() + " is valid");
		} catch (SAXException e) {
		  System.out.println(xmlFile.getSystemId() + " is NOT valid reason:" + e);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		/* SimpleHandlerError handler = new SimpleHandlerError();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);

        SchemaFactory schemaFactory = 
            SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema%22");

        factory.setSchema(schemaFactory.newSchema(
            new Source[] {new StreamSource(schema.getFile())}));

        javax.xml.parsers.SAXParser parser = factory.newSAXParser();
        XMLReader reader = parser.getXMLReader();
        reader.setErrorHandler(handler);
        reader.parse(new InputSource( new StringReader( xml ) ));
        if (handler.hasError() == true) {
            throw new SAXException(handler.getError());
        }*/
        
		//Marshall
		Cvi cvi;
		try {
			cvi = unmarshal(xml);
			bdd.put( getUnusedUUID() , cvi);
			System.out.println(cvi.toString());
		} catch (JAXBException e) {
			throw new HTTPException(500);
		}
		
		for( Entry<UUID, Cvi> entry : bdd.entrySet()) {
			if(cvi.equals(entry.getValue())) return entry.getKey().toString();
		}
		throw new HTTPException(404);
		
	}
	
	private UUID getUnusedUUID() {
		UUID uuid;
		do {
			uuid = UUID.randomUUID();
		} while(bdd.containsKey(uuid));
		return uuid;
	}

	private Cvi unmarshal(String xml) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Cvi.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (Cvi) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)));  
    }
	
	private String marshal(Cvi cv) throws JAXBException {
        StringWriter stringWriter = new StringWriter();

        JAXBContext jaxbContext = JAXBContext.newInstance(Cvi.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
            true);

        QName qName = new QName("", "cvi");
        JAXBElement<Cvi> root = new JAXBElement<Cvi>(qName, Cvi.class, cv);

        jaxbMarshaller.marshal(root, stringWriter);

        return stringWriter.toString();
      }

	@RequestMapping(value = "/list/{id}")
	@ResponseBody
	public String getListId(@PathVariable("id") int id) {
		if (id < 2) {
			return "Demande limitée à 2 cvs";
		} else {
			return new CVS("bombadil", "tom").toString();
		}
	}
}